# NativeScript-Vue Application

> A native application built with NativeScript-Vue

## Usage

``` bash
# Install dependencies
npm install

# Preview on device
tns preview

# Build, watch for changes and run the application
tns run

# Build, watch for changes and debug the application
tns debug <platform>

# Build for production
tns build <platform> --env.production

# Routing method used
Manual routing is used, instead of vue router

# Pages created
1) AddBlogs.vue
2) ListBlogs.vue
3) ReadBlogs.vue
4) Login.vue

# TS files used
app/main.ts
app/store.ts
app/shared/services/JSONFileSystemService.ts
app/shared/services/firebase-sync.service.ts
app/shared/contants.ts
app/shared/ScreenDimensions.ts
app/shared/values.ts

#JSON file used
app/assets/data/blogs.json

# Dependencies used
@nativescript/theme
@vue/devtools
nativescript-camera
nativescript-image-cache-it
nativescript-imagepicker
nativescript-permissions
nativescript-plugin-firebase
nativescript-ui-listview
nativescript-vue
tns-core-modules
vuex

# Dev Dependencies used
@babel/core
@babel/preset-env
babel-loader
nativescript-dev-webpack
nativescript-vue-template-compiler
nativescript-worker-loader
node-sass
tns-platform-declarations
typescript
@types/node
vue
vue-loader



```
