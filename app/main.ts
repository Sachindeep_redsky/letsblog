import { Constants } from './shared/constants';
import Vue from 'nativescript-vue'
import ListBlogs from './pages/ListBlogs.vue'
import Login from './pages/Login.vue'
import store from './store'
import RadListView from 'nativescript-ui-listview/vue';
import { Values } from './shared/values';
import { ImageCacheIt } from 'nativescript-image-cache-it';

Vue.use(RadListView);

Vue.registerElement('ImageCacheIt', () => ImageCacheIt)

// Prints Vue logs when --env.production is *NOT* set while building
Vue.config.silent = (TNS_ENV === 'production')

var userStr = Values.readString(Values.USER, '');

if (userStr) {
  initializeRoute(ListBlogs);
  let user = JSON.parse(userStr)
  if (user && user.role == "editor") {
    Constants.IS_ADMIN = true;
  }
} else {
  console.log("NO user");
  Constants.IS_ADMIN = false;
  initializeRoute(Login);
}

function initializeRoute(route) {
  new Vue({
    store,
    render: h => h('frame', [h(route)])
  }).$start()
}