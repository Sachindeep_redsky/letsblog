export class Constants {

    public static JSON_FILE_PATH = "./assets/data/blogs.json";
    public static IMAGE_ASSETS_PATH = "./assets/images";
    public static STORAGE_BUCKET_PATH = "gs://let-s-blog.appspot.com";
    public static BLOG_LIST = [];
    public static hasLocal = false;
    public static IS_ADMIN = false;

}