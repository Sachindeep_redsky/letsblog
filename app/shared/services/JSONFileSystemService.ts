import * as fs from "tns-core-modules/file-system";
let documents = fs.knownFolders.currentApp();

export class JSONFileSystemService {

    public static readJSON(path: string): Promise<Object> {
        let jsonFile = documents.getFile(path);
        return new Promise<Object>((resolve, reject) => {
            jsonFile.readText().then((content: string) => {
                let data = <Array<Object>>JSON.parse(content);
                resolve(data);
            })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    public static writeJSON(path: string, content: string): Promise<any> {

        let jsonFile = documents.getFile(path);
        return new Promise<boolean>((resolve, reject) => {
            jsonFile.writeText(content).then((result) => {
                jsonFile.readText()
                    .then((res) => {
                        resolve(true)
                    });
            })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    public static appendToJSON(path: string, content: any): Promise<any> {

        return new Promise<any>((resolve, reject) => {
            JSONFileSystemService.readJSON(path).then(res => {
                var blogs = <Array<any>>res["blogs"].body;
                blogs.push(content);

                var contentObj = {
                    "blogs": {
                        "body": blogs
                    }
                }
                JSONFileSystemService.writeJSON(path, JSON.stringify(contentObj))
                resolve(true)
            },
                err => {
                    reject(err);
                })
        })
    }

};
