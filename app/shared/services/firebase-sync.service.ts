import { storage, push, setValue } from 'nativescript-plugin-firebase';
import { File } from '@nativescript/core/file-system';
import { isIOS } from 'tns-core-modules/ui/frame/frame';

declare var FIRDatabase: any;

export class FirebaseSync {

    public static BLOGS_TO_SYNC: any = [];
    public static SYNCED_BLOGS: any = [];

    public static checkAndSync() {

        for (var i = 0; i < FirebaseSync.BLOGS_TO_SYNC.length; i++) {
            let file = File.fromPath(FirebaseSync.BLOGS_TO_SYNC[i].image);

            var imageName = file.path.substr(file.path.lastIndexOf("/") + 1);
            let index = i;
            FirebaseSync.uploadImage(file, imageName).then(
                uploadedImage => {
                    this.getDownloadUrl(imageName).then(
                        (url) => {
                            FirebaseSync.BLOGS_TO_SYNC[index].image = url;
                            FirebaseSync.pushBlog(FirebaseSync.BLOGS_TO_SYNC[index]).then((result) => {
                                FirebaseSync.SYNCED_BLOGS.push(FirebaseSync.BLOGS_TO_SYNC[index]);
                                FirebaseSync.BLOGS_TO_SYNC.splice(index, 1);
                            }, error => {
                                console.log("Could not save at server:::", error);
                            })
                        },
                        error => {
                            console.log("Could not get download url:::", error);
                        }
                    );
                },
                error => {
                    console.log("Could not save the image at server:::", error);
                }
            );
        }
    }

    public static uploadImage(localFile: File, imageName: string): Promise<any> {

        var metadata = {
            contentType: "image/png",
            contentLanguage: "en"
        };

        return new Promise((resolve, reject) => {
            storage
                .uploadFile({
                    remoteFullPath: `uploads/images/${imageName}`,
                    localFile: localFile,
                    onProgress: status => {
                        console.log("Uploaded fraction: " + status.fractionCompleted);
                        console.log("Percentage complete: " + status.percentageCompleted);
                    },
                    metadata: metadata
                })
                .then(
                    uploadedFile => {
                        resolve(uploadedFile);
                    },
                    error => {
                        reject(error);
                    }
                );
        });
    }

    public static getDownloadUrl(imageName: string): Promise<any> {
        return new Promise((resolve, reject) => {
            storage
                .getDownloadUrl({
                    remoteFullPath: `uploads/images/${imageName}`
                })
                .then(
                    url => {
                        resolve(url);
                    },
                    error => {
                        reject(error);
                    }
                );
        });
    }

    public static pushBlog(blog: any): Promise<any> {

        //Don't send ID on server
        let pushBlog = {
            name: blog.name,
            title: blog.title,
            author: blog.author,
            content: blog.content,
            image: blog.image,
        };

        return new Promise((resolve, reject) => {
            push("/blogs", pushBlog).then(
                result => {
                    resolve(result)
                },
                error => {
                    reject(error)
                }
            );
        });
    }

    public static setUser(user: any): Promise<any> {

        return new Promise((resolve, reject) => {

            if (isIOS) {
                try {
                    FIRDatabase.database().reference().child('users').child(user.uid).setValueWithCompletionBlock({ 'user': JSON.stringify(user) }, function (error, dbRef) {
                        error ? reject(error.localizedDescription) : resolve();
                    });
                }
                catch (ex) {
                    console.log("Error in firebase.setValue: " + ex);
                    reject(ex);
                }
            } else {
                try {
                    setValue(`/users/${user.uid}`, { 'user': JSON.stringify(user) }).then((result) => {
                        resolve(result);
                    })
                }
                catch (ex) {
                    console.log("Error in firebase.setValue: " + ex);
                    reject(ex);
                }
            }
        });
    }

}